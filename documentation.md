# FabZero @ FabLab ULB - Documenting your work


## Goal of this unit

The goal of this unit is to learn some easy but powerful tools to write better documentation and share it using version control system.

## Table of contents

[[_TOC_]]

##  Why is documentation important ?

* documenting the learning process (failures and success)
* it is not about the final result, it's about the process
* document for your future self - the goldfish memory
* share to help others

## Examples

* [Jake Read, MIT CBA - Tensile Test Machine](https://gitlab.cba.mit.edu/jakeread/displacementexercise)
* [Lasersaur, an open-source laser cutter](https://www.lasersaur.com/)
* [ULB FabZero-Design](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/class-website/)
* Fab Academy: [Gilles Decroly](https://fabacademy.org/2019/labs/ulb/students/gilles-decroly/), [Nicolas De Coster](http://archive.fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/index.html), [Denis Terwagne](http://archive.fabacademy.org/archives/2017/woma/students/238/)


## How can you document what you are doing ?

### Text editor

* Text editor (open-source, buil-in Markdown preview)
  * [Atom](https://atom.io/)
  * [Visual Studio Code](https://code.visualstudio.com/)
  * ...

* useful features
  * open-source
  * Markdown Built in preview
  * Code compiler (Latex, Python,...)

> **Learn by doing** : Install a text editor

### Writing documentation in Markdown

[FabZero tutorial](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#writing-documentation-in-markdown)

> **Learn by doing** : Create a markdown page with a text editor and introduce yourself. Use header titles, bold text, italic text, link, bullets and an image.

### Typical Markdown to HTML workflow

pandoc, css, script -> [FabZero tutorial](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#typical-markdown-to-html-workflow)

> **Learn by doing** : (optional) Install pandoc and convert a markdown file to an html file

### Images

* [GraphicsMagick](http://www.graphicsmagick.org/) - [examples here](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#pictures)
    * resize `gm convert -resize 600x600 bigimage.png smallimage.jpg`
    * images strip
      * left to right, 400px height
          `gm convert +append -geometry x400 image1.png image2.png image3.png image4.png strip.png`
      * top to bottom, 400 px width
          `gm convert -append -geometry 400x image1.png image2.png image3.png image4.png strip.png`
* [ImageMagick](https://imagemagick.org/index.php) - [Encoding examples](http://academy.cba.mit.edu/classes/computer_design/image.html)
* [GIMP](https://www.gimp.org/downloads/)
    * [compress](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-filesize-of-a-jpeg)
    * [resize](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-dimensions-of-an-image-scale)
    * [crop](https://www.gimp.org/tutorials/GIMP_Quickies/#crop-an-image)
    * [batch](https://alessandrofrancesconi.it/projects/bimp/)

> **Learn by doing** : Upload an image of yourself, crop it, resize it, compress it and add it to your markdown document.

### Videos

[FabZero tutorial here](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md#video)

* Screen recording
    * [SimpleScreenRecorder](http://www.maartenbaert.be/simplescreenrecorder/)

* Record, convert and stream
  * [FFMPEG](https://ffmpeg.org/)


* [Video formats](https://blog.mynd.com/en/mp4-mov-avi-more-9-video-formats-you-need-to-know)
    * GIF
    * mp4, preferred format for online videos
    * mov
    * avi

* Download videos from YouTube using [youtube-dl](https://youtube-dl.org/)
      `youtube-dl -f 'best[filesize<5M]' https://www.youtube.com/watch?v=tn7Wg9zQVTg`


* [IMPORTANT - Encoding - Cheat sheet !](http://academy.cba.mit.edu/classes/computer_design/video.html)

* Embedding a video located in your local repository in a webpage (HTML5)

      `<video width="500" controls src="files/video.mp4">
      video description
      </video>`

<video width="500" controls src="files/sing_bamboleo.mp4">
video description
</video>

* [Embed a Youtube video](https://support.google.com/youtube/answer/171780?hl=fr)

      `<iframe width="560" height="315" src="https://www.youtube.com/embed/tn7Wg9zQVTg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`


> **Learn by doing** :  (optional) capture a video (screen recorder, phone, download from YouTube....) and compress, resize and convert it to another format. So you get roughly 10Mo/min.

## More useful open-source programs

Post-processing Audio and Video :
* Audio -[Audacity](https://www.audacityteam.org/)
* Video editing - [kdenlive](https://kdenlive.org/fr/)
* video conversion - [Handbrake](https://handbrake.fr/)

## Version control - GIT & GitLab

### GitLab

#### Update your doc and your website through the Gitlab.com GUI

* Sign-in on [gitlab.com](https://gitlab.com/) using your username and password
* you will find your project space on the class repo : [https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments)
*  opening your project, you will find all the information on how to modify your website.

#### Activate gitlab notification by emails

To receive gitlab notification by emails, [change your "global notification level" to "watch"](https://docs.gitlab.com/ee/user/profile/notifications.html#editing-notification-settings). If you have a lots of projects going on, you may want to specify which "project", you want to "watch".

### Git

#### Use Git to be more efficient

Use Git to clone and work on a "working copy" of your remote project repository on your local computer

#### Why ?

* Working on a local "working copy" allows you to make all the modification you need on a local version on your computer before you "commit" to the distant server.
* It is much much much faster than using the gitlab GUI
* As it is fast and easy, you can "document as you go"
* It allows you to work collaboratively with many people on a shared project at the same time.

#### How to setup and use Git on your computer ?

Here are the major steps :

##### 1. Install and configure GIT on your computer. Setup a secure connection with a remote server.

*one time per laptop*

* [Install GIT on your computer](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git)
* [configure GIT](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git)
* [set up SSH keys to connect your local computer to the remote server (such as gitlab.com) in a secured way](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)  

##### 2. Clone a working copy of a remote projet on your computer.

*one time per project*

* [git clone via SSH](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-via-ssh)

##### 3. Work on a working copy  and synchronize with the remote server.
*Everyday*  

* [Download the latest changes in the project](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#download-the-latest-changes-in-the-project)  
		   ` git pull`
* [Add all changes to commit](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#add-all-changes-to-commit)  
		   `git add -A`  
       `git commit -m "COMMENT TO DESCRIBE THE INTENTION OF THE COMMIT"`  
* [Send changes to remote server](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#send-changes-to-gitlabcom)  
		   `git push`

### File names

Working on your project, you might encounter some problems from broken links (for exemple an image, a file or a page you cannot access when you click on the link).

Be aware, many Web Hosting Hubs servers are linux-based which means they are they case-sensitive. Some other os are not case-sensitive, thus it is always better [to use a simple an universal way of naming files](https://www.webhostinghub.com/help/learn/website/website-help/use-case-sensitivity-file-names).

(optional) For research, here are some tips for [naming research data files](https://library.stanford.edu/research/data-management-services/data-best-practices/best-practices-file-naming), particularly useful when you take a lot of data.

## Acknowledge the work of others

!!!!

## Having problems ?

Use the issue tracker in the class coordination project [here](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class).


## Going further

* [FabZero Basics](https://github.com/Academany/fabzero/tree/master/program/basic)
* [Learn more about GIT here](https://github.com/Academany/fabzero/tree/master/program/basic/git.md)

## Assignment

* build a personal site in the class archive describing you.
* document your learning path for this module.

## Credits
This unit is build-on the [Official FabZero Program](https://github.com/Academany/fabzero/blob/master/program/basic/doc.md).
